const btn = document.getElementById('clear_btn');

btn.addEventListener('click', function handleClick(event) {
  // 👇️ if you are submitting a form
  event.preventDefault();

  const inputs = document.querySelectorAll('#department_in, #key_input');

  inputs.forEach(input => {
    input.value = '';
  });
});
